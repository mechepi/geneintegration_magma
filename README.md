
<!-- README.md is generated from README.Rmd. Please edit that file -->

# geneintegration_magma

<!-- badges: start -->
<!-- badges: end -->

The goal of geneintegration_magma project is to utilise a compendium of
transcriptomic datasets for a comparative analyses between generalised
and focal epileptic seizure models. This study integrates the
transcriptomics and GWAS data addtionally to identify gene-sets which
have significant overlap with the common single nucleotide polymorphisms
(SNPs) and to highlight protein protein interactions (PPIs) specific to
different regions of the brain.

### Directory structure

![](figures/pipe_str.png "workflow")

The setup of the project is as follows

    #>                           levelName
    #> 1  GeneIntegration_magma           
    #> 2   ¦--_targets                    
    #> 3   ¦--run.R                       
    #> 4   ¦--renv.lock                   
    #> 5   ¦--data                        
    #> 6   ¦   °--raw                     
    #> 7   ¦--src                         
    #> 8   ¦   ¦--DE                      
    #> 9   ¦   °--magma                   
    #> 10  ¦--tools                       
    #> 11  ¦   °--MAGMA_tool              
    #> 12  °--results                     
    #> 13      °--magma                   
    #> 14          ¦--generalised_epilepsy
    #> 15          ¦--focal_epilepsy      
    #> 16          °--intermediate

### Installation requirements

The pipeline has been generated using targets(Landau 2021) workflow.
Please visit <https://github.com/ropensci/targets> for documentation.
For reproducing our analysis using this pipeline, the following external
installations are required,

-   **MAGMA**: It is available from
    <https://ctg.cncr.nl/software/magma>. Version 1.9 of this software
    has been used for this comparative study. If you use it, please cite
    their publication (Leeuw et al. 2015).

-   During the second part of the analysis, interaction with the
    humanbase software is required. Please follow this link to
    interactively use the *functional module detection*
    :<https://hb.flatironinstitute.org/>

-   **GSCluster**: This R package (Yoon et al. 2019)
    <https://github.com/unistbig/GScluster> has been used for
    implementing fuzzy clustering for detecting the PPIs.

#### Transcriptomics references

Please follow this shinyapp to see the transcriptomic datasets used for
this analysis: <https://sreyoshi.shinyapps.io/shiny/>

*Note*: Many datasets were received through collaboration and are not
permitted to be shared freely. Please contact the corresponding authors
for those datasets. Links have been provided in the app for the publicly
available datasets.

#### GWAS references

GWAS summary statistics were downloaded from :
<https://www.ebi.ac.uk/gwas/studies/GCST007343> (Consortium et al. 2018)

*Note*: ‘Genetic generalized epilepsy’ and ‘Focal epilepsy’ categories
were used for the analysis.

### Workflow details

The pipeline has been made reproducible by using renv. After cloning
from git, please install the renv package first and run
`renv::restore()` to get all the packages and dependencies used in the
project.

#### Datasets and their dependencies

The major data sets can be retrieved in bulk. For internal use, softlink
the ownCloud share `P2/` to `data/DE/raw`.

    cd data/DE/
    ln -s  ~/ownCloud/P2/data/DE/raw raw

There should not be a `raw` directory previously existing.

The raw GWAS data used in this analysis can be found in ownCloud under
the GWAS folder.

    cd data/
    ln -s ~/owncloud/P2/data/gwas gwas

For installing MAGMA, please download version 1.9 of the appropriate
compiler based on the operating system from website
(<https://ctg.cncr.nl/software/magma>) and place it under the */tools*
folder.

    cd ~/your_path_to/geneintegration_magma
    mkdir tools
    cd tools/
    paste your magma installer here
    cd magma_souce_v1.9

For MAC OS

    make install

Please follow the MAGMA documentation for installation in Linux and
Windows operating systems.

The MAGMA software also requires usage of the 1000Genomes files and
NCBI’s gene locations background informations. The versions used in this
study can be accessed using,

    cd data/
    ln -s ~/ownCloud/P2/data/magma magma

#### Running the pipeline

Once all the datasets and other files have been linked from ownCloud, it
is recommended to run the pipeline can be run by

    source(run.R)

You can also choose to run different parts of the pipeline by using
commands for example,

    tar_make(names=ends_with("report"))
    tar_make()

For more information, please have a look at
[targets](https://books.ropensci.org/targets/walkthrough.html) for
details of this workflow management system.

The raw data of SCN2A is comparatively large and computation speed can
be increased by editing the renviron file with the following commands

    library(usethis)
    usethis::edit_r_environ()
    R_MAX_VSIZE=100Gb

### Further analysis

Apart from the targets pipeline, PPI weighted gene-set clustering was
done on the significant gene-sets as highlighted by MAGMA. To run the
GSCluster tool (see installation requirements) for reproducing the
results in the manuscript, please run the script *gscluster.R* in an
interactive fashion. The required gene-set results can be found at
*/results/magma/gene_sets* and the gene scores can be obtained from the
pipeline results under the *results_magma* target.

### License

### Contact

For questions or comments, please contact <sreyoshi.chatterjee@uni.lu>

### References

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-consortium2018genome" class="csl-entry">

Consortium, The International League Against Epilepsy et al. 2018.
“Genome-Wide Mega-Analysis Identifies 16 Loci and Highlights Diverse
Biological Mechanisms in the Common Epilepsies.” *Nature Communications*
9.

</div>

<div id="ref-targets" class="csl-entry">

Landau, William Michael. 2021. “The Targets r Package: A Dynamic
Make-Like Function-Oriented Pipeline Toolkit for Reproducibility and
High-Performance Computing.” *Journal of Open Source Software* 6 (57):
2959. <https://doi.org/10.21105/joss.02959>.

</div>

<div id="ref-de2015magma" class="csl-entry">

Leeuw, Christiaan A de, Joris M Mooij, Tom Heskes, and Danielle
Posthuma. 2015. “MAGMA: Generalized Gene-Set Analysis of GWAS Data.”
*PLoS Comput Biol* 11 (4): e1004219.

</div>

<div id="ref-yoon2019gscluster" class="csl-entry">

Yoon, Sora, Jinhwan Kim, Seon-Kyu Kim, Bukyung Baik, Sang-Mun Chi,
Seon-Young Kim, and Dougu Nam. 2019. “GScluster: Network-Weighted
Gene-Set Clustering Analysis.” *BMC Genomics* 20 (1): 1–14.

</div>

</div>
